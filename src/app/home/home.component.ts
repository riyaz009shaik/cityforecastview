import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { CityDataService } from '../city-data.service';


//Importing async module for calling end point multiple times
import * as async from 'async';
import { Constants } from '../constants';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  cityData: any = [];

  //Importing list of city names and country codes
  inputData = Constants.avilableCities;
  citiesInfo: any = [];
  constructor(
    private cityDataService: CityDataService,
    private router: Router,
    private zone: NgZone
  ) {}

  ngOnInit() {
    //Calling method to retrive data from service
    this.retriveData();
  }

  //Getting data asynchonusly from api
  retriveData() {
    //Async mapping
    async.map(
      this.inputData,
       (obj: any, callback: any) => {
        this.cityDataService
          .getCityData(obj.cityName, obj.countryCode)
          .then((res) => {
            callback(null, res);
          });
      },
      (error: any, finalRes: any) => {
        if (!error) {
          //Updating data in view
          this.zone.run(() => {
            this.citiesInfo = finalRes;
          });
        } else {
          alert('Something went wrong..');
        }
      }
    );
  }

  //Navigating to details component
  viewDetails(data: any) {
    this.router.navigate(['weather-details'], {
      state: {
        data: data,
      },
    });
  }

}
