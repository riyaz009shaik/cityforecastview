import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CitydetailsComponent} from '../app/citydetails/citydetails.component';
import {HomeComponent} from '../app/home/home.component';


//Declaring app routing
const routes: Routes = [
  {path:"weather-details", component:CitydetailsComponent},
  {path:'home',component: HomeComponent},
  {path:'',component: HomeComponent}  // Default route
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
