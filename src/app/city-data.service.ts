import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from './constants';

@Injectable({
  providedIn: 'root',
})
export class CityDataService {
  city: String = '';
  countryCode: String = '';
  constructor(private http: HttpClient) {}


  // Getting Baisc city weather data 
  getCityData(city: any, countryCode: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http
        .get(

          //Constructing API end point from contants and dynamic variable (City name and Country Code)
          Constants.baseUrl +
            city +
            ',' +
            countryCode +
            '&appid=' +
            Constants.apiKey
        )
        .subscribe((res) => {
          if (res) {
            resolve(res);
          } else {
            reject();
          }
        });
    });
  }

  //Getting 5 days weather forecast data of a city
  getForeCastApiData(id: any) {
    return new Promise((resolve, reject) => {
      this.http
        .get(

          //Constructing API end point from contants and dynamic variable (City ID)
          Constants.forecast5baseUrl +
            '/forecast?id=' +
            id +
            '&appid=' +
            Constants.apiKey
        )
        .subscribe((res) => {
          if (res) {
            resolve(res);
          } else {
            reject();
          }
        });
    });
  }


}
