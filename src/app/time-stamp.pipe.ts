import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeStamp'
})
export class TimeStampPipe implements PipeTransform {
//converting UTC time to human readable time
  transform(time: any): any {
     var localTime = new Date(time * 1000);
     var newTime =  (localTime.toLocaleString('en-GB', { timeZone: 'UTC' }));
     var res = newTime.split(",");
    var finalResult =  res[1].split(':') ;
     return   finalResult[0] + ":" + finalResult[1];     
  }

}
