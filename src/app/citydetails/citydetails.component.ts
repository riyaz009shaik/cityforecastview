import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CityDataService } from '../city-data.service';

@Component({
  selector: 'app-citydetails',
  templateUrl: './citydetails.component.html',
  styleUrls: ['./citydetails.component.css'],
})
export class CitydetailsComponent implements OnInit {
  cityData: any = {};
  forecatsData: any = [];
  constructor(
    private router: Router,
    private cityDataService: CityDataService
  ) {}

  ngOnInit(): void {

    //Checking routing history
    if (history.state) {

      if (history.state.data) {
        this.cityData = history.state.data;

        //Getting 5 days weather forecast data of a city
        this.cityDataService
          .getForeCastApiData(this.cityData.id)
          .then((res: any) => {
            
            this.forecatsData = res.list;
            
          })
          .catch((err) => {
            alert('Unable to fetch forecast data');
          });

      } else {

        //Navigating back to default route if history state not found
        this.router.navigate(['/home']);
      }
    }
  }

  //Checking time stamp 9AM or not
  checkTime(timestamp: any) {
    let str = timestamp.split(' ');
    let hours = str[1].slice(0, 2);
    if (hours == '09') {
      return true;
    } else {
      return false;
    }
  }
 
}
