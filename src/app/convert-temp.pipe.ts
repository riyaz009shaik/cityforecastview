import { Pipe, PipeTransform } from '@angular/core';
import {Constants} from '../app/constants'

@Pipe({
  name: 'convertTemp'
})
export class ConvertTempPipe implements PipeTransform {

  //Converting temparature from Kelving to Celsius
  transform(kelvin:any): any {
    return (kelvin-Constants.kelvinMeasurement).toFixed(1);
  }

}
