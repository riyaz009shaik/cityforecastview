import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

import { CityDataService } from './city-data.service';
import { HttpClientModule } from '@angular/common/http';
import { CitydetailsComponent } from './citydetails/citydetails.component';
import { ConvertTempPipe } from './convert-temp.pipe';
import { TimeStampPipe } from './time-stamp.pipe';

@NgModule({
  declarations: [AppComponent, HomeComponent, CitydetailsComponent, ConvertTempPipe, TimeStampPipe],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [CityDataService],
  bootstrap: [AppComponent],
})
export class AppModule {}
