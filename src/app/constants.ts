// Defining Global Contants 
export const Constants= {
   baseUrl:"http://api.openweathermap.org/data/2.5/weather?q=",  // 
   apiKey:"3d8b309701a13f65b660fa2c64cdc517",
   forecast5baseUrl:"http://api.openweathermap.org/data/2.5/",
   avilableCities:[
      {cityName: "Paris", countryCode: "fr"},
      {cityName: "London", countryCode: "uk"},
      {cityName: "Madrid", countryCode: "es"},
      {cityName: "Berlin", countryCode: "De"},
      {cityName: "Rome", countryCode: "it"}
    ],
    kelvinMeasurement: 273.15
}